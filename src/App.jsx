import './App.css';
import CvPage from './pages/cvPage/CvPage';
import MainPage from './pages/mainPage/MainPage';
import { Route, Routes, Navigate } from 'react-router-dom';
import { panelRoutes } from './core/config/routes.config';

function App() {
    return (
        <Routes>
            <Route path={panelRoutes.getMainRoute()} element={<MainPage />}>
                <Route index element={<Navigate to="/cv" replace />} />
                <Route index path={panelRoutes.getCvRoute()} element={<CvPage />} />
            </Route>
        </Routes>
    );
}

export default App;
