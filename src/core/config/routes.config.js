export const AppRoutes = {
    MAIN_PAGE: "/",
}

export const AppPanelMatchRoutes = {
    CV_PAGE: "/cv",
}

export const panelRoutes = {
    getMainRoute: () => `${AppRoutes.MAIN_PAGE}`,
    getCvRoute: () => `${AppRoutes.MAIN_PAGE}${AppPanelMatchRoutes.CV_PAGE}`,
};