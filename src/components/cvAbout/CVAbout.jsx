import "./cvAbout.css"

const CVAbout = () => {
    return (
        <div className="cv-page__about" id="about">
            <div className="about__greeting-cnt">
                <div className="greeting__ellipse">
                    <div className="greeting__text">Привет!</div>
                </div>
                <div className="greeting__name">Меня зовут Михаил.</div>
                <div className="greeting__description">
                    Я <font color="#FFF">web-</font>разработчик<font color="#FFF">.</font>
                </div>
            </div>
            <div className="about__selfie-block">
                <div className="about__selfie"></div>
            </div>
        </div>
    )
}

export default CVAbout;