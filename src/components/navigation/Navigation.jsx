import "./navigation.css";
import { Link } from "react-scroll";

const Navigation = () => {

    return (
        <div className="navigation">
            <Link className="navigation__link" to="about" activeClass="active" spy={true} smooth={true} duration={500} offset={0}>
                <div className="navigation__inner">
                    <div className="navigation__icon"></div>
                    <div className="navigation__text">Приветствие</div>
                </div>
            </Link>
            <Link className="navigation__link" to="story" activeClass="active" spy={true} smooth={true} duration={500} offset={0}>
                <div className="navigation__inner">
                    <div className="navigation__icon"></div>
                    <div className="navigation__text">История</div>
                </div>
            </Link>
            <Link className="navigation__link" to="video" activeClass="active" spy={true} smooth={true} duration={500} offset={0}>
                <div className="navigation__inner">
                    <div className="navigation__icon"></div>
                    <div className="navigation__text">Видео</div>
                </div>
            </Link>
            <Link className="navigation__link" to="contacts" activeClass="active" spy={true} smooth={true} duration={500} offset={0}>
                <div className="navigation__inner">
                    <div className="navigation__icon"></div>
                    <div className="navigation__text">Контакты</div>
                </div>
            </Link>
        </div>
    )
}

export default Navigation;