import "./cvVideo.css"
import video from "./videos/css.mp4"

const CVVideo = () => {
    return (
        <div className="cv-page__video" id="video">
            <div className="video__tv">
                <video className="tv__player" controls >
                    <source src={video} type="video/mp4" />
                </video>
            </div>
        </div>
    )
}

export default CVVideo;