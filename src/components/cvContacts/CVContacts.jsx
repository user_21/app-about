import "./cvContacts.css"

const CVContacts = () => {
    return (
        <div className="cv-page__contacts" id="contacts">
            <div className="contacts__hand"></div>
            <div className="contacts__text">Надеюсь, до скорого :)</div>
            <div className="contacts__photo"></div>
            <div className="contacts__contacts-block">
                <div className="contacts__left-flex-block">
                    <a href="https://t.me/Michael_Chusov">
                        <div className="contacts__social-media contacts__telegram"></div>
                    </a>
                    <a href="https://wa.me/+79036556433">
                        <div className="contacts__social-media contacts__whatsapp"></div>
                    </a>
                </div>
                <div className="contacts__right-flex-block">
                    <div className="contacts__repo">
                        <a className="contacts__repo-href" href="https://gitlab.com/user_21/app-about">
                            Репозиторий: https://gitlab.com/user_21/app-about
                        </a>
                    </div>
                    <div className="contacts__email">E-mail: chi.chi.09@mail.ru</div>
                </div>
            </div>
        </div>

    )
}

export default CVContacts;