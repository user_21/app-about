import "./mainPage.css";
import Navigation from "../../components/navigation/Navigation";
import { Outlet } from "react-router-dom";

const MainPage = () => {
    return (
        <div className="main-page-wrapper">
            <Navigation />
            <div className="main-page__content">
                <Outlet />
            </div>
        </div>
    );
}

export default MainPage;