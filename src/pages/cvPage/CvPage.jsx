import CVAbout from "../../components/cvAbout/CVAbout";
import CVStory from "../../components/cvStory/CVStory";
import CVVideo from "../../components/cvVideo/CVVideo";
import CVContacts from "../../components/cvContacts/CVContacts";
import "./cvPage.css";

const CvPage = () => {
    return (
        <div className="cv-page">
            <CVAbout />
            <CVStory />
            <CVVideo />
            <CVContacts />
        </div>
    )
}

export default CvPage;